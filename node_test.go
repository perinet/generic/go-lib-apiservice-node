/*
 * Copyright (c) Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

package node

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"
	"time"
	"unsafe"

	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gitlab.com/perinet/generic/lib/utils/shellhelper"
	"gitlab.com/perinet/generic/lib/utils/webhelper"
	"gitlab.com/perinet/periMICA-container/apiservice/node/nodeErrorStatus"
	"gitlab.com/perinet/periMICA-container/apiservice/node/nodeLifeState"
	"gotest.tools/v3/assert"
)

func TestMain(m *testing.M) {
	dir := "_cache/var/lib/node-service"
	os.RemoveAll(dir)
	prefixPATH = "_cache"
	shellhelper.WriteFile(prefixPATH+"/META/configuration.json", `
{
"interface_version": "2.0.0",
"version": "23",
"container": "container name",
"support": "support@perinet.io",
"maintainer": "user.name@perinet.io"
}
`)
	SetServiceConfig(ServiceConfig{StorageBasePath: dir})
	os.Exit(m.Run())
}

var toJSON = func(status any) []byte {
	data, _ := json.Marshal(status)
	return data
}

var callback_A_Called bool
var callback_B_Called bool

var nodeCallback_A = func() {
	callback_A_Called = true
}

var nodeCallback_B = func() {
	callback_B_Called = true
}

func TestNodeInfo(t *testing.T) {
	var nodeInfo NodeInfo
	data := intHttp.Get(NodeInfoGet, nil)
	err := json.Unmarshal(data, &nodeInfo)
	assert.Assert(t, err == nil)

	assert.Assert(t, nodeInfo.ApiVersion == API_VERSION)
	assert.Assert(t, nodeInfo.ErrorStatus == nodeErrorStatus.NO_ERROR)
	assert.Assert(t, nodeInfo.LifeState == nodeLifeState.OEM)
	assert.Assert(t, nodeInfo.Firmware.Name == "container name")
	assert.Assert(t, nodeInfo.Firmware.Version == "23")
	assert.Assert(t, nodeInfo.Firmware.Specifier == "")
}

func TestProductionInfoStorage(t *testing.T) {
	var err error
	now := time.Now()
	// remove files
	os.Remove(productionInfoPath)
	productionInfo_cache.ProductVersion = now.String()
	// store change load
	err = productionInfo_cache.store()
	assert.Assert(t, err == nil)

	productionInfo_cache = defaultProductionInfo
	// load back
	err = productionInfo_cache.load()
	assert.Assert(t, err == nil)

	assert.Assert(t, productionInfo_cache.ProductVersion == now.String())
}

func TestNodeConfigStorage(t *testing.T) {
	var err error
	now := time.Now()

	os.Remove(nodeConfigPath)

	nodeConfig_cache.ApplicationName = now.String()
	nodeConfig_cache.ElementName = now.String()

	err = nodeConfig_cache.store()
	assert.Assert(t, err == nil)

	nodeConfig_cache = defaultNodeConfig

	// load back
	err = nodeConfig_cache.load()
	assert.Assert(t, err == nil)

	assert.Assert(t, nodeConfig_cache.ApplicationName == now.String())
	assert.Assert(t, nodeConfig_cache.ElementName == now.String())
}

func TestServices(t *testing.T) {
	var services []string
	var err error

	services_json := intHttp.Get(ServicesGet, nil)
	err = json.Unmarshal(services_json, &services)
	assert.Assert(t, err == nil)
	assert.Assert(t, len(services) == 0)

	services = []string{"node", "security", "lifecycle", "mqttclient"}
	services_json, err = json.Marshal(services)
	assert.Assert(t, err == nil)
	intHttp.Put(ServicesSet, services_json, nil)
	data := intHttp.Get(NodeInfoGet, nil)
	var nodeInfo NodeInfo
	err = json.Unmarshal(data, &nodeInfo)
	assert.Assert(t, err == nil)
	assert.Assert(t, len(nodeInfo.Services) == 4)
}

func TestConfig(t *testing.T) {
	var err error
	var data []byte
	callback_A_Called = false
	callback_B_Called = false
	RegisterNodeEventCallback(nodeCallback_A)
	RegisterNodeEventCallback(nodeCallback_B)

	data = intHttp.Get(NodeConfigGet, nil)
	var nodeConfig NodeConfig
	err = json.Unmarshal(data, &nodeConfig)
	assert.Assert(t, err == nil)
	assert.Assert(t, nodeConfig == nodeConfig_cache)

	nodeConfig.ApplicationName = "testApplicationName"
	nodeConfig.ElementName = "testElementName"
	data, err = json.Marshal(nodeConfig)
	assert.Assert(t, err == nil)
	intHttp.Patch(NodeConfigSet, data, nil)

	data = intHttp.Get(NodeConfigGet, nil)

	var nC NodeConfig
	err = json.Unmarshal(data, &nC)
	assert.Assert(t, err == nil)
	assert.Assert(t, nC == nodeConfig)

	assert.Assert(t, callback_A_Called == true)
	assert.Assert(t, callback_B_Called == true)
	callback_A_Called = false
	callback_B_Called = false

	assert.Assert(t, nC.ApplicationName != defaultNodeConfig.ApplicationName)
	assert.Assert(t, nC.ElementName != defaultNodeConfig.ElementName)
	webhelper.InternalDelete(NodeConfigReset)
	assert.Assert(t, callback_A_Called == true)
	assert.Assert(t, callback_B_Called == true)
	callback_A_Called = false
	callback_B_Called = false
	data = intHttp.Get(NodeConfigGet, nil)
	err = json.Unmarshal(data, &nC)
	assert.Assert(t, err == nil)
	assert.Assert(t, nC.ApplicationName == defaultNodeConfig.ApplicationName)
	assert.Assert(t, nC.ElementName == defaultNodeConfig.ElementName)

}

func TestProductionInfo(t *testing.T) {
	var err error
	var data []byte
	callback_A_Called = false
	callback_B_Called = false

	data = intHttp.Get(ProductionInfoGet, nil)
	var productionInfo ProductionInfo
	err = UnmarshalWithAdditionalProperties(data, &productionInfo)
	assert.Assert(t, err == nil)
	assert.Assert(t, productionInfo.BatchNumber == productionInfo_cache.BatchNumber)
	assert.Assert(t, productionInfo.Hostname == productionInfo_cache.Hostname)
	assert.Assert(t, productionInfo.Manufacturer == productionInfo_cache.Manufacturer)
	assert.Assert(t, productionInfo.Mpn == productionInfo_cache.Mpn)
	assert.Assert(t, productionInfo.ProductName == productionInfo_cache.ProductName)
	assert.Assert(t, productionInfo.ProductVariant == productionInfo_cache.ProductVariant)
	assert.Assert(t, productionInfo.ProductVersion == productionInfo_cache.ProductVersion)

	productionInfo.Hostname = "testHostname"
	productionInfo.BatchNumber = "22-41"
	data, err = json.Marshal(productionInfo)
	assert.Assert(t, err == nil)


	// prepare for patch production info
	state_production := nodeLifeState.PRODUCTION
	intHttp.Put(LifeStateSet, toJSON(state_production), nil)

	intHttp.Patch(ProductionInfoSet, data, nil)
	assert.Assert(t, callback_A_Called == true)
	assert.Assert(t, callback_B_Called == true)
	callback_A_Called = false
	callback_B_Called = false

	data = intHttp.Get(ProductionInfoGet, nil)

	var pI ProductionInfo
	err = json.Unmarshal(data, &pI)
	assert.Assert(t, err == nil)
	assert.Assert(t, pI.BatchNumber == productionInfo.BatchNumber)
	assert.Assert(t, pI.Hostname == productionInfo.Hostname)
	assert.Assert(t, pI.Manufacturer == productionInfo.Manufacturer)
	assert.Assert(t, pI.Mpn == productionInfo.Mpn)
	assert.Assert(t, pI.ProductName == productionInfo.ProductName)
	assert.Assert(t, pI.ProductVariant == productionInfo.ProductVariant)
	assert.Assert(t, pI.ProductVersion == productionInfo.ProductVersion)
}

func TestProductionInfoPatch(t *testing.T) {
	var err error
	var data []byte
	callback_A_Called = false
	callback_B_Called = false

	data = intHttp.Get(ProductionInfoGet, nil)
	var productionInfo ProductionInfo
	err = json.Unmarshal(data, &productionInfo)
	assert.Assert(t, err == nil)

	var nodeconfig NodeConfig
	nodeconfig.ApplicationName = "test"
	data, err = json.Marshal(nodeconfig)
	assert.Assert(t, err == nil)
	intHttp.Patch(NodeConfigSet, data, nil)
	assert.Assert(t, callback_A_Called == true)
	assert.Assert(t, callback_B_Called == true)
	callback_A_Called = false
	callback_B_Called = false

	data = intHttp.Get(ProductionInfoGet, nil)
	var newproductionInfo ProductionInfo
	err = json.Unmarshal(data, &newproductionInfo)
	assert.Assert(t, err == nil)

	assert.Assert(t, productionInfo.BatchNumber == newproductionInfo.BatchNumber)
	assert.Assert(t, productionInfo.Hostname == newproductionInfo.Hostname)
	assert.Assert(t, productionInfo.Manufacturer == newproductionInfo.Manufacturer)
	assert.Assert(t, productionInfo.Mpn == newproductionInfo.Mpn)
	assert.Assert(t, productionInfo.ProductName == newproductionInfo.ProductName)
	assert.Assert(t, productionInfo.ProductVariant == newproductionInfo.ProductVariant)
	assert.Assert(t, productionInfo.ProductVersion == newproductionInfo.ProductVersion)
}

func TestLifeState(t *testing.T) {
	callback_A_Called = false
	callback_B_Called = false
	state1 := nodeLifeState.PRODUCTION
	state2 := nodeLifeState.UPDATED

	intHttp.Put(LifeStateSet, toJSON(state1), nil)
	assert.Assert(t, nodeInfo_cache.LifeState == state1)
	assert.Assert(t, callback_A_Called == true)
	assert.Assert(t, callback_B_Called == true)
	callback_A_Called = false
	callback_B_Called = false
	intHttp.Put(LifeStateSet, toJSON(state2), nil)
	assert.Assert(t, nodeInfo_cache.LifeState == state2)
	assert.Assert(t, callback_A_Called == true)
	assert.Assert(t, callback_B_Called == true)
	callback_A_Called = false
	callback_B_Called = false
}

func TestStatusOrder(t *testing.T) {
	assert.Assert(t, nodeInfo_cache.ErrorStatus == nodeErrorStatus.NO_ERROR)

	statusList := []NodeErrorStatus{nodeErrorStatus.NO_ERROR, nodeErrorStatus.WARNING, nodeErrorStatus.ERROR, nodeErrorStatus.CRITICAL, nodeErrorStatus.FATAL}
	var usedStatusList []NodeErrorStatus
	for _, status := range statusList {
		intHttp.Put(ErrorStatusSet, toJSON(status), nil)
		assert.Assert(t, nodeInfo_cache.ErrorStatus == status)
		for _, usedStatus := range usedStatusList {
			intHttp.Put(ErrorStatusSet, toJSON(usedStatus), nil)
		}
		usedStatusList = append(usedStatusList, status)
	}
}

func TestStatusReset(t *testing.T) {
	callback_A_Called = false
	callback_B_Called = false

	intHttp.Put(ErrorStatusSet, toJSON(nodeErrorStatus.FATAL), nil)

	assert.Assert(t, nodeInfo_cache.ErrorStatus == nodeErrorStatus.FATAL)

	intHttp.Put(ErrorStatusReset, []byte{}, nil)
	assert.Assert(t, callback_A_Called == true)
	assert.Assert(t, callback_B_Called == true)
	callback_A_Called = false
	callback_B_Called = false
	assert.Assert(t, nodeInfo_cache.ErrorStatus == nodeErrorStatus.NO_ERROR)
}

func TestUnRegisterEvent(t *testing.T) {
	var fn_a = func() {}
	var fn_b = func() {}
	found_fn_a := false
	found_fn_b := false
	RegisterNodeEventCallback(fn_a)
	RegisterNodeEventCallback(fn_b)
	for _, v := range nodeEventList {
		p1 := *(*unsafe.Pointer)(unsafe.Pointer(&v))
		p2 := *(*unsafe.Pointer)(unsafe.Pointer(&fn_a))
		p3 := *(*unsafe.Pointer)(unsafe.Pointer(&fn_b))
		if p1 == p2 {
			found_fn_a = true
		}
		if p1 == p3 {
			found_fn_b = true
		}
	}
	assert.Assert(t, found_fn_a == true)
	assert.Assert(t, found_fn_b == true)
	found_fn_a = false
	found_fn_b = false
	UnegisterNodeEventCallback(fn_a)
	for _, v := range nodeEventList {
		p1 := *(*unsafe.Pointer)(unsafe.Pointer(&v))
		p2 := *(*unsafe.Pointer)(unsafe.Pointer(&fn_a))
		p3 := *(*unsafe.Pointer)(unsafe.Pointer(&fn_b))
		if p1 == p2 {
			found_fn_a = true
		}
		if p1 == p3 {
			found_fn_b = true
		}
	}
	assert.Assert(t, found_fn_a == false)
	assert.Assert(t, found_fn_b == true)
	found_fn_a = false
	found_fn_b = false
	UnegisterNodeEventCallback(fn_b)
	for _, v := range nodeEventList {
		p1 := *(*unsafe.Pointer)(unsafe.Pointer(&v))
		p2 := *(*unsafe.Pointer)(unsafe.Pointer(&fn_a))
		p3 := *(*unsafe.Pointer)(unsafe.Pointer(&fn_b))
		if p1 == p2 {
			found_fn_a = true
		}
		if p1 == p3 {
			found_fn_b = true
		}
	}
	assert.Assert(t, found_fn_a == false)
	assert.Assert(t, found_fn_b == false)
}


func TestProductionDataExtended(t *testing.T) {
	
	os.Remove(productionInfoPath)

	prodTest := ProductionInfo{
       Manufacturer: "Perinet GmbH",
        Mpn: "PRN.999.999",
        SerialNumber: "sernm",
        ProductVersion: "1",
        ProductVariant: "test",
        BatchNumber: "2024-20",
        ProductName: "nononononono",
        Hostname: "periCORE-sernm",
        IsFused: true,
        WithAdditionalProperties: WithAdditionalProperties{
            AdditionalProperties: map[string]interface{}{
                "pp1": "value1",
                "pp2": 21,
				"pp3": true,
        },}}

	prodTest_json, err := json.Marshal(prodTest)
	assert.Assert(t, err == nil)
	intHttp.Patch(ProductionInfoSet, prodTest_json, nil)

	data := intHttp.Get(ProductionInfoGet, nil)

	var productionInfo ProductionInfo
	err = UnmarshalWithAdditionalProperties(data, &productionInfo)
	assert.Assert(t, err == nil)

	expectedAdditionalProperties := map[string]interface{}{
        "pp1": "value1",
        "pp2": 21,
		"pp3": true,
    }
	
	for key, expectedValue := range expectedAdditionalProperties {
        actualValue, ok := productionInfo.AdditionalProperties[key]
        assert.Assert(t, ok, "Key '%s' should exist in AdditionalProperties", key)
        
        expectedValueStr := fmt.Sprintf("%v", expectedValue)
        actualValueStr := fmt.Sprintf("%v", actualValue)

        assert.Assert(t, expectedValueStr == actualValueStr, "Value for key '%s' should be '%v', got '%v'", key, expectedValueStr, actualValueStr)
    }

    assert.Assert(t, len(productionInfo.AdditionalProperties) == len(expectedAdditionalProperties))
}