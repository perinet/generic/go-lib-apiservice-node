# apiserviceNode

The apiservice Node is the implementation of the node part from the
[unified api](https://gitlab.com/perinet/unified-api). The api service node is
available in all Perinets Products. 

This implementation does store its resources, e.g. NodeInfo object or the ProductionInfo object persistently in the file system `/var/lib/apiservice-node/` as json files.

## References

* [openAPI spec for apiservice-node] https://gitlab.com/perinet/unified-api/-/blob/main/services/_Node_openapi.yaml


# Dual License

This software is by default licensed via the GNU Affero General Public License
version 3. However it is also available with a commercial license on request
(https://perinet.io/contact).