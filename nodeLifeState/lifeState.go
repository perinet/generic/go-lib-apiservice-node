/*
 * Copyright (c) Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

package nodeLifeState

import (
	"encoding/json"
)

type Enum int

const (
	PRODUCTION Enum = iota
	OEM
	UPDATED
)

var stringMap = map[Enum]string{
	PRODUCTION: "PRODUCTION",
	OEM:        "OEM",
	UPDATED:    "UPDATED",
}

func (level Enum) String() string {
	return stringMap[level]
}

func (l Enum) MarshalJSON() ([]byte, error) {
	return json.Marshal(l.String())
}

func (l *Enum) UnmarshalJSON(data []byte) error {
	var stringLevel string

	if err := json.Unmarshal(data, &stringLevel); err != nil {
		return err
	}

	for i, entry := range stringMap {
		if entry == stringLevel {
			*l = i
			return nil
		}
	}
	return &json.UnsupportedValueError{}
}
