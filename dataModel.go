/*
 * Copyright (c) 2018-2022 Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

package node

import (
	"encoding/json"
	"os"
	"reflect"
	"strings"

	"gitlab.com/perinet/generic/lib/utils/filestorage"
	"gitlab.com/perinet/periMICA-container/apiservice/node/nodeErrorStatus"
	"gitlab.com/perinet/periMICA-container/apiservice/node/nodeLifeState"
)

// NodeInfo - Basic Node information
type NodeInfo struct {
	ApiVersion json.Number `json:"api_version"`

	Config *NodeConfig `json:"config"`

	LifeState NodeLifeState `json:"life_state"`

	Services []string `json:"services"`

	ErrorStatus NodeErrorStatus `json:"error_status"`

	Firmware FirmwareInfo `json:"firmware"`
}

type NodeErrorStatus = nodeErrorStatus.Enum

type NodeConfig struct {
	ApplicationName string `json:"application_name"`
	ElementName     string `json:"element_name"`
}

type WithAdditionalProperties struct {
    AdditionalProperties map[string]interface{} `json:"-"`
}

type ProductionInfo struct {

	WithAdditionalProperties // accept additional properties added during production

	Manufacturer string `json:"manufacturer"`

	// Manufacturer Part Number
	Mpn string `json:"mpn"`

	SerialNumber string `json:"serial_number"`

	ProductVersion string `json:"product_version"`

	ProductVariant string `json:"product_variant"`

	// Manufacturer production batch identification
	BatchNumber string `json:"batch_number"`

	ProductName string `json:"product_name"`

	Hostname string `json:"hostname"`

	IsFused bool `json:"is_fused"`

}

type NodeLifeState = nodeLifeState.Enum

type FirmwareInfo struct {
	Version   string `json:"version"`
	Name      string `json:"name"`
	Specifier string `json:"specifier"`
}

type ContainerMetaInfo struct {
	Version   string `json:"version"`
	Container string `json:"container"`
	Support   string `json:"support"`
	Mantainer string `json:"mantainer"`
}

var defaultNodeInfo = NodeInfo{ApiVersion: API_VERSION, Config: &nodeConfig_cache, LifeState: nodeLifeState.OEM, Services: []string{}, ErrorStatus: NodeErrorStatus(nodeErrorStatus.NO_ERROR), Firmware: FirmwareInfo{}}
var defaultNodeConfig = NodeConfig{ApplicationName: "", ElementName: ""}
var defaultProductionInfo = newDefaultProductionInfo()

var nodeInfo_cache = defaultNodeInfo
var nodeConfig_cache = defaultNodeConfig
var productionInfo_cache = defaultProductionInfo

func newDefaultProductionInfo() ProductionInfo {
	host, err := os.Hostname()
	if err != nil {
		host = ""
	}
	productionInfo := ProductionInfo{
		Manufacturer:   "",
		Mpn:            "",
		SerialNumber:   "",
		ProductVersion: "",
		ProductVariant: "",
		BatchNumber:    "",
		ProductName:    "",
		Hostname:       host,
		IsFused: 		false,
	}
	return productionInfo
}

var (
	productionInfoPath = "/var/lib/apiservice-node/productionInfo.json"
	nodeConfigPath     = "/var/lib/apiservice-node/nodeConfig.json"
)

func (obj ProductionInfo) store() error {
	return filestorage.StoreObject(productionInfoPath, obj)
}

func (obj *ProductionInfo) load() error {
	return filestorage.LoadObject(productionInfoPath, obj)
}

func (obj NodeConfig) store() error {
	return filestorage.StoreObject(nodeConfigPath, obj)
}

func (obj *NodeConfig) load() error {
	return filestorage.LoadObject(nodeConfigPath, obj)
}


func (p ProductionInfo) MarshalJSON() ([]byte, error) {

    result := map[string]interface{}{}

    val := reflect.ValueOf(p)
    typ := val.Type()

    for i := 0; i < val.NumField(); i++ {
        field := typ.Field(i)
        jsonTag := field.Tag.Get("json")
        if jsonTag == "" || jsonTag == "-" {
            continue
        }
        result[jsonTag] = val.Field(i).Interface()
    }

    for key, value := range p.AdditionalProperties {
        result[key] = value
    }

    return json.Marshal(result)
}

func UnmarshalWithAdditionalProperties(data []byte, target interface{}) error {

	var rawMap map[string]interface{}
	if err := json.Unmarshal(data, &rawMap); err != nil {
		return err
	}

	val := reflect.ValueOf(target).Elem()
	typ := val.Type()

	for i := 0; i < typ.NumField(); i++ {
		field := typ.Field(i)
		jsonTag := field.Tag.Get("json")
		if jsonTag == "" || jsonTag == "-" {
			continue
		}

		if commaIdx := strings.Index(jsonTag, ","); commaIdx != -1 {
			jsonTag = jsonTag[:commaIdx]
		}

		if value, exists := rawMap[jsonTag]; exists {
			fieldValue := val.Field(i)
			if fieldValue.CanSet() {
				jsonBytes, err := json.Marshal(value)
				if err != nil {
					return err
				}
				if err := json.Unmarshal(jsonBytes, fieldValue.Addr().Interface()); err != nil {
					return err
				}
				delete(rawMap, jsonTag)
			}
		}
	}

	additionalProperties := val.FieldByName("WithAdditionalProperties").FieldByName("AdditionalProperties")
	if additionalProperties.IsNil() {
		additionalProperties.Set(reflect.MakeMap(additionalProperties.Type()))
	}
	for k, v := range rawMap {
		additionalProperties.SetMapIndex(reflect.ValueOf(k), reflect.ValueOf(v))
	}

	return nil
}

