module gitlab.com/perinet/periMICA-container/apiservice/node

go 1.22.0

toolchain go1.22.3

require (
	gitlab.com/perinet/generic/lib/httpserver v1.0.1
	gitlab.com/perinet/generic/lib/utils v1.0.1
	gotest.tools/v3 v3.5.1
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.4 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/handlers v1.5.2 // indirect
	github.com/gorilla/mux v1.8.1 // indirect
	golang.org/x/exp v0.0.0-20241108190413-2d47ceb2692f // indirect
)
