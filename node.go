/*
 * Copyright (c) Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

// Container dedicated implementation for the API service Node of Perinets
// unified API. It provides node specific meta information, general
// funding configuration values like application_name and element_name as well as
// production information
package node

import (
	"encoding/json"
	"log"
	"net/http"
	"unsafe"

	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/httpserver/periHttp"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"
	"gitlab.com/perinet/generic/lib/utils/filestorage"
	"gitlab.com/perinet/periMICA-container/apiservice/node/nodeErrorStatus"
	"gitlab.com/perinet/periMICA-container/apiservice/node/nodeLifeState"
)

type PathInfo = httpserver.PathInfo

// Get a list of public available paths
func PathsGet() []PathInfo {
	return []PathInfo{
		{Url: "/node", Method: httpserver.GET, Role: rbac.NONE, Call: NodeInfoGet},
		{Url: "/node/config", Method: httpserver.GET, Role: rbac.USER, Call: NodeConfigGet},
		{Url: "/node/config", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: NodeConfigSet},
		{Url: "/node/config", Method: httpserver.DELETE, Role: rbac.ADMIN, Call: NodeConfigReset},
		{Url: "/node/production", Method: httpserver.GET, Role: rbac.ADMIN, Call: ProductionInfoGet},
		{Url: "/node/production", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: ProductionInfoSet},
		{Url: "/node/services", Method: httpserver.GET, Role: rbac.NONE, Call: ServicesGet},
		{Url: "/node/services", Method: httpserver.PUT, Role: rbac.INTERNAL, Call: ServicesSet},
		{Url: "/node/life_state", Method: httpserver.PUT, Role: rbac.INTERNAL, Call: LifeStateSet},
		{Url: "/node/status", Method: httpserver.PUT, Role: rbac.INTERNAL, Call: ErrorStatusSet},
		{Url: "/node/status", Method: httpserver.DELETE, Role: rbac.INTERNAL, Call: ErrorStatusReset},
	}
}

type ServiceConfig struct {
	FirmwareInfo    FirmwareInfo
	StorageBasePath string
}

type NodeEventCallback func()

const (
	API_VERSION = "23"
)

var (
	prefixPATH    = ""
	nodeEventList []NodeEventCallback
)

func init() {
	SetServiceConfig(ServiceConfig{StorageBasePath: "/var/lib/apiservice-node"})
}

func RegisterNodeEventCallback(cb NodeEventCallback) {
	for _, v := range nodeEventList {
		p1 := *(*unsafe.Pointer)(unsafe.Pointer(&v))
		p2 := *(*unsafe.Pointer)(unsafe.Pointer(&cb))
		if p1 == p2 {
			// nothing to be added, already in list
			return
		}
	}
	nodeEventList = append(nodeEventList, cb)
}

func UnegisterNodeEventCallback(cb NodeEventCallback) {
	for i, v := range nodeEventList {
		p1 := *(*unsafe.Pointer)(unsafe.Pointer(&v))
		p2 := *(*unsafe.Pointer)(unsafe.Pointer(&cb))
		if p1 == p2 {
			nodeEventList = append(nodeEventList[:i], nodeEventList[i+1:]...)
			return
		}
	}
}

func SetServiceConfig(serviceConfig ServiceConfig) {
	var err error

	firmwareInfoChanged := false
	if serviceConfig.FirmwareInfo.Name != "" {
		nodeInfo_cache.Firmware.Name = serviceConfig.FirmwareInfo.Name
		firmwareInfoChanged = true
	}
	if serviceConfig.FirmwareInfo.Version != "" {
		nodeInfo_cache.Firmware.Version = serviceConfig.FirmwareInfo.Version
		firmwareInfoChanged = true
	}
	if serviceConfig.FirmwareInfo.Specifier != "" {
		nodeInfo_cache.Firmware.Specifier = serviceConfig.FirmwareInfo.Specifier
		firmwareInfoChanged = true
	}
	if serviceConfig.StorageBasePath != "" {
		productionInfoPath = serviceConfig.StorageBasePath + "/productionInfo.json"
		nodeConfigPath = serviceConfig.StorageBasePath + "/nodeConfig.json"
	}

	//load persistent data
	err = nodeConfig_cache.load()
	if err != nil {
		log.Println("SetServiceConfig: nodeConfig load error: ", err)
	}

	err = productionInfo_cache.load()
	if err != nil {
		log.Println("SetServiceConfig: productionInfo load error: ", err)
	}

	// restore Firmware information from former '/META/configuration.json'
	if !firmwareInfoChanged {
		var containerMetaInfo ContainerMetaInfo

		err := filestorage.LoadObject(prefixPATH+"/META/configuration.json", &containerMetaInfo)
		if err == nil {
			SetServiceConfig(ServiceConfig{FirmwareInfo: FirmwareInfo{Name: containerMetaInfo.Container, Version: containerMetaInfo.Version}})
		}
	}
}

func NodeInfoGet(p periHttp.PeriHttp) {
	p.JsonResponse(http.StatusOK, nodeInfo_cache)
}

func NodeConfigGet(p periHttp.PeriHttp) {
	p.JsonResponse(http.StatusOK, nodeConfig_cache)
}

func notifyChange() {
	for _, nodeEvent := range nodeEventList {
		nodeEvent()
	}
}

func NodeConfigSet(p periHttp.PeriHttp) {
	payload, _ := p.ReadBody()

	newConfig := nodeConfig_cache

	err := json.Unmarshal(payload, &newConfig)
	if err != nil {
		log.Println("NodeConfigSet error: ", err)
		p.EmptyResponse(http.StatusBadRequest)
		return
	}

	nodeConfig_cache = newConfig

	notifyChange()

	err = nodeConfig_cache.store()
	if err != nil {
		log.Println("error on storing nodeConfig: ", err)
		p.EmptyResponse(http.StatusInternalServerError)
		return
	}

	p.EmptyResponse(http.StatusNoContent)
}

func NodeConfigReset(p periHttp.PeriHttp) {
	// restore to defatuls
	nodeConfig_cache = defaultNodeConfig

	notifyChange()

	err := nodeConfig_cache.store()
	if err != nil {
		log.Println("error on storing nodeConfig: ", err)
		p.EmptyResponse(http.StatusInternalServerError)
		return
	}

	p.EmptyResponse(http.StatusNoContent)
}

func ProductionInfoGet(p periHttp.PeriHttp) {
	p.JsonResponse(http.StatusOK, productionInfo_cache)
}

func ProductionInfoSet(p periHttp.PeriHttp) {
	payload, _ := p.ReadBody()

	newProductionInfo := productionInfo_cache

	err := UnmarshalWithAdditionalProperties(payload, &newProductionInfo)
	if err != nil {
		log.Println("ProductionInfoSet error: ", err)
		p.EmptyResponse(http.StatusBadRequest)
		return
	}

	// only allow update in production if the life state is PRODUCTION and the node is not fused yet
	if productionInfo_cache.IsFused || nodeInfo_cache.LifeState != nodeLifeState.PRODUCTION {
		p.EmptyResponse(http.StatusConflict) // return conflict code: 409
	} else {
		productionInfo_cache = newProductionInfo

		notifyChange()

		err = productionInfo_cache.store()
		if err != nil {
			log.Println("error on storing productionInfo: ", err)
			p.EmptyResponse(http.StatusInternalServerError)
			return
		}

		p.EmptyResponse(http.StatusNoContent)
	}
}

func ServicesGet(p periHttp.PeriHttp) {
	p.JsonResponse(http.StatusOK, nodeInfo_cache.Services)
}

func ServicesSet(p periHttp.PeriHttp) {
	payload, _ := p.ReadBody()

	newServices := nodeInfo_cache.Services

	err := json.Unmarshal(payload, &newServices)
	if err != nil {
		log.Println("ServicesSet error: ", err)
		p.EmptyResponse(http.StatusBadRequest)
		return
	}
	nodeInfo_cache.Services = newServices

	p.EmptyResponse(http.StatusNoContent)
}

func LifeStateSet(p periHttp.PeriHttp) {
	data, _ := p.ReadBody()
	var newNodeLifeState NodeLifeState
	err := json.Unmarshal(data, &newNodeLifeState)
	if err != nil {
		log.Println("LifeStateSet error: ", err)
		p.EmptyResponse(http.StatusBadRequest)
		return
	}
	if !(newNodeLifeState >= nodeLifeState.PRODUCTION && newNodeLifeState <= nodeLifeState.UPDATED) {
		p.EmptyResponse(http.StatusBadRequest)
		return
	}
	nodeInfo_cache.LifeState = newNodeLifeState

	notifyChange()

	p.EmptyResponse(http.StatusNoContent)
}

func ErrorStatusSet(p periHttp.PeriHttp) {
	data, _ := p.ReadBody()
	var newNodeStatus NodeErrorStatus
	err := json.Unmarshal(data, &newNodeStatus)
	if err != nil {
		p.EmptyResponse(http.StatusBadRequest)
		return
	}
	// verify data validity
	if !(newNodeStatus >= nodeErrorStatus.NO_ERROR && newNodeStatus <= nodeErrorStatus.FATAL) {
		p.EmptyResponse(http.StatusBadRequest)
		return
	}

	if nodeInfo_cache.ErrorStatus < newNodeStatus {
		nodeInfo_cache.ErrorStatus = newNodeStatus
	}

	p.EmptyResponse(http.StatusNoContent)
}

func ErrorStatusReset(p periHttp.PeriHttp) {
	nodeInfo_cache.ErrorStatus = nodeErrorStatus.NO_ERROR

	notifyChange()

	p.EmptyResponse(http.StatusNoContent)
}
